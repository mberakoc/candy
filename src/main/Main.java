package main;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import ui.buttons.ToggleButton;
import utils.AppProperty;

public class Main extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(new StackPane(new ToggleButton(ToggleButton.Size.MEDIUM)), 600, 400);
        AppProperty.instance.scene = scene;
        stage.setTitle("Candy");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
