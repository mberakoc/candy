package utils;

public class AppColor {

    public static final String IOS_GREEN = "#5ADA4E";
    public static final String IOS_GRAY = "#FBFAFB";
    public static final String IOS_STROKE_COLOR = "#C2C1C2";
}
