package utils;

import javafx.scene.Cursor;
import javafx.scene.Node;
import main.Main;

public class Events {

    public static Events instance = new Events();

    private Events() {}

    public void addHoverEvent(Node node) {
        node.setOnMouseEntered(e -> AppProperty.instance.scene.setCursor(Cursor.HAND));
        node.setOnMouseExited(e -> AppProperty.instance.scene.setCursor(Cursor.DEFAULT));
    }
}
