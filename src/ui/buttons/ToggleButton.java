package ui.buttons;

import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Parent;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import utils.AppColor;
import utils.Events;

public class ToggleButton extends Parent {

    public enum Size {
        SMALL(25), MEDIUM(50), BIG(100);

        private int size;

        private Size(int size) {
            this.size = size;
        }

        public int getSize() {
            return size;
        }
    }

    private BooleanProperty isSwitchedOn = new SimpleBooleanProperty(false);
    private TranslateTransition triggerAnimation = new TranslateTransition(Duration.seconds(0.25));
    private FillTransition backgroundAnimation = new FillTransition(Duration.seconds(0.25));
    private ParallelTransition animationStorage = new ParallelTransition(triggerAnimation, backgroundAnimation);

    public boolean isIsSwitchedOn() {
        return isSwitchedOn.get();
    }

    public BooleanProperty isSwitchedOnProperty() {
        return isSwitchedOn;
    }

    public ToggleButton(Size size) {
        int injectedSize = size.getSize();
        Rectangle background = new Rectangle(injectedSize * 2, injectedSize);
        background.setArcWidth(injectedSize);
        background.setArcHeight(injectedSize);
        background.setFill(Color.WHITE);
        background.setStroke(Color.LIGHTGRAY);
        backgroundAnimation.setShape(background);
        Circle trigger = new Circle(injectedSize / 2.1f);
        trigger.setCenterX(injectedSize / 2f);
        trigger.setCenterY(injectedSize / 2f);
        trigger.setFill(Color.WHITE);
        trigger.setStroke(Color.WHITE);
        Events.instance.addHoverEvent(trigger);
        DropShadow shadow = new DropShadow(BlurType.THREE_PASS_BOX, Color.rgb(0, 0, 0, 0.4),
                3 * injectedSize / 50f, -(injectedSize / 50f), 0, injectedSize / 50f);
        trigger.setEffect(shadow);
        triggerAnimation.setNode(trigger);
        getChildren().addAll(background, trigger);
        isSwitchedOn.addListener((obs, oldState, newState) -> {
            boolean isOn = newState;
            triggerAnimation.setToX(isOn ? injectedSize : 0);
            backgroundAnimation.setToValue(isOn ? Color.web(AppColor.IOS_GREEN) : Color.WHITE);
            animationStorage.play();
        });
        setOnMouseClicked(e -> {
            isSwitchedOn.set(!isSwitchedOn.get());
        });
    }
}
